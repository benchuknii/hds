const expect = require('chai').expect;
const Bim360dmParser = require('../parsers/bim360dmParser');
const ResultUtil = require('../parsers/resultUtil');

describe('Bim360dmParser() data with all good endpoints', function () {
    it('should run parser and get result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new Bim360dmParser();


        // 2. ACT
        const res = p.parse(bim360dmParserGoodData, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.GOOD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("bim360-dm");
        expect(res.time).to.be.equal("2018-08-22T23:20:31.056Z");
        expect(res.errors.length).to.be.equal(0);

    });
});

describe('Bim360dmParser() data with bad endpoints', function () {
    it('should run parser and get result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new Bim360dmParser();


        // 2. ACT
        const res = p.parse(bim360dmParserBadData, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.BAD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("bim360-dm");
        expect(res.time).to.be.equal("2018-08-22T23:20:31.056Z");
        expect(res.errors.length).to.be.equal(5);

    });
});

describe('Bim360dmParser() data with empty data', function () {
    it('should run parser and get BAD result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new Bim360dmParser();


        // 2. ACT
        const res = p.parse(bim360dmParserEmptyData, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.BAD);
        expect(res.url).to.be.equal(url);
        expect(res.errors.length).to.be.equal(1);

    });
});


describe('Bim360dmParser() undefined data', function () {
    it('should run parser and fail [throw]', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new Bim360dmParser();

        // 2. ACT

        // 3. ASSERT
        expect(function () { p.parse(undefined, url) } ).to.throw();

    });
});

var bim360dmParserBadData = {
    "service": "bim360-dm", "time": "2018-08-22T23:20:31.056Z",
    "environment": "production",
    "status": {
        "overall": "GOOD",
        "active_record": "GOOD",
        "redis": "GOOD",
        "last_schema_version": "20180822021222",
        "db_connection_count": 71,
        "self_machine_connection_count": 0,
        "self_process_connection_count": 2,
        "workers": [
            {
                "bim360-dm-worker-dev-09a697005e5fddc14":
                    { "issue_ping": "BAD", "dos_ping": "BAD", "folder_notification": "BAD", "acm": "BAD", "oss": "BAD" }
            },
        ], "i18n": { "en": "Settings", "en-GB": "Settings", "de": "Einstellungen", "es": "Configuración", "fr": "Paramètres", "ja": "設定", "pt": "Configurações", "zh-Hans": "设置", "nl": "Instellingen", "sv": "Inställningar", "hr": "Settings" }
    }, "more_information": "please always refer to detailed health page for system health", "build": { "revision": "ae4a829", "deploy_date": "2018-08-22T15:22:12.000+00:00" }, "sid": "dm", "host": "bim360-dm-app-dev-0b251ab73358a7b70", "request_duration": 0.169354878
};

var bim360dmParserGoodData = {
    "service": "bim360-dm", "time": "2018-08-22T23:20:31.056Z",
    "environment": "production",
    "status": {
        "overall": "GOOD",
        "active_record": "GOOD",
        "redis": "GOOD",
        "last_schema_version": "20180822021222",
        "db_connection_count": 71,
        "self_machine_connection_count": 0,
        "self_process_connection_count": 2,
        "workers": [
            {
                "bim360-dm-worker-dev-09a697005e5fddc14":
                    { "issue_ping": "GOOD", "dos_ping": "GOOD", "folder_notification": "GOOD", "acm": "GOOD", "oss": "GOOD" }
            },
        ], "i18n": { "en": "Settings", "en-GB": "Settings", "de": "Einstellungen", "es": "Configuración", "fr": "Paramètres", "ja": "設定", "pt": "Configurações", "zh-Hans": "设置", "nl": "Instellingen", "sv": "Inställningar", "hr": "Settings" }
    }, "more_information": "please always refer to detailed health page for system health", "build": { "revision": "ae4a829", "deploy_date": "2018-08-22T15:22:12.000+00:00" }, "sid": "dm", "host": "bim360-dm-app-dev-0b251ab73358a7b70", "request_duration": 0.169354878
};


var bim360dmParserEmptyData = {};
