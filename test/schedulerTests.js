const expect = require('chai').expect;
const Scheduler = require('../modules/scheduler');

describe('Scheduler() test that called in time', function () {
    it('should call worker', function (done) {

        // 1. ARRANGE
        let data = [];
        const worker = function()
        {
            data.push("called once");
        };
        const s = new Scheduler(110,worker,"testWorker");        
        // 2. ACT
        s.start(this);
        
        // 3. ASSERT
        setTimeout( function () {
            try {
                expect(data.length).to.be.equal(1);
              done(); 
            } catch( e ) {
              done( e ); 
            }
          }, 120);
    });
});

describe('Scheduler() test that did not called too early', function () {
    it('should NOT call worker', function (done) {

        // 1. ARRANGE
        let data = [];
        const worker = function()
        {
            data.push("called once");
        };
        const s = new Scheduler(300,worker,"testWorker");        
        // 2. ACT
        s.start(this);
        
        // 3. ASSERT
        setTimeout( function () {
            try {
                expect(data.length).to.be.equal(0);
              done(); 
            } catch( e ) {
              done( e ); 
            }
          }, 120);
    });
});

describe('Scheduler() test that did not called after stop ', function () {
    it('should NOT call worker', function (done) {

        // 1. ARRANGE
        let data = [];
        const worker = function()
        {
            data.push("called once");
        };
        const s = new Scheduler(150,worker,"testWorker");        
        // 2. ACT
        s.start(this);
        
        setTimeout( function () {
            try {
                s.stop();
            } catch( e ) {
              
            }
          }, 120);

        // 3. ASSERT
        setTimeout( function () {
            try {
                expect(data.length).to.be.equal(0);
              done(); 
            } catch( e ) {
              done( e ); 
            }
          }, 170);
    });
});

describe('Scheduler() test bad init 1', function () {
    it('should throw', function () {

        // 1. ARRANGE
           
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new Scheduler(100,undefined,"testWorker");  } ).to.throw();
    });
});


describe('Scheduler() test bad init 2', function () {
    it('should throw', function () {

        // 1. ARRANGE
           
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new Scheduler(1000, undefined ,"testWorker");  } ).to.throw();
    });
});

describe('Scheduler() test bad init 3', function () {
    it('should throw', function () {

        // 1. ARRANGE
           
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new Scheduler(1000, {} ,"testWorker");  } ).to.throw();
    });
});