const expect = require('chai').expect;
const CommandsParser = require('../parsers/commandsParser');
const ResultUtil = require('../parsers/resultUtil');

describe('CommandsParser() data with all good endpoints', function () {
    it('should run parser and get result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new CommandsParser();

        // 2. ACT
        const res = p.parse(commandsDataGood, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.GOOD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("Command Processor");
        expect(res.time).to.be.equal("2018-08-23T20:26:37.769Z");
        expect(res.errors.length).to.be.equal(0);

    });
});

describe('CommandsParser() data with bad endpoints', function () {
    it('should run parser and get BAD result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new CommandsParser();


        // 2. ACT
        const res = p.parse(commandsDataBad, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.BAD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("Command Processor");
        expect(res.time).to.be.equal("2018-08-23T20:26:37.769Z");
        expect(res.errors.length).to.be.equal(1);

    });
});

describe('CommandsParser() data with bad endpoints 2', function () {
    it('should run parser and get BAD result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new CommandsParser();


        // 2. ACT
        const res = p.parse(commandsDataBad2, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.BAD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("Command Processor");
        expect(res.time).to.be.equal("2018-08-23T20:26:37.769Z");
        expect(res.errors.length).to.be.equal(2);

    });
});

describe('CommandsParser() data with invalid data', function () {
    it('should run parser and should throw', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new CommandsParser();
       
        // 2. ACT
        // 3. ASSERT
        expect(function () { p.parse(commandsDataIMissingStatusInvalidFormat, url) } ).to.throw();

    });
});

describe('CommandsParser() data with empty data', function () {
    it('should run parser and should throw', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new CommandsParser();
       
        // 2. ACT
        // 3. ASSERT
        expect(function () { p.parse(commandsDataIEmpty, url) } ).to.throw();

    });
});


describe('CommandsParser() undefined data', function () {
    it('should run parser and fail [throw]', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new CommandsParser();

        // 2. ACT
        // 3. ASSERT
        expect(function () { p.parse(undefined, url) } ).to.throw();

    });
});

var commandsDataGood = {"time":"2018-08-23T20:26:37.769Z","service":"Command Processor","environment":"development","host":"N/A","status":{"db":{"status":"OK"},"overall":"OK"},"build":"48a26eda\n0.0.0-2268\n","duration":5};
var commandsDataBad = {"time":"2018-08-23T20:26:37.769Z","service":"Command Processor","environment":"development","host":"N/A","status":{"db":{"status":"BAD"},"overall":"OK"},"build":"48a26eda\n0.0.0-2268\n","duration":5};
var commandsDataBad2 = {"time":"2018-08-23T20:26:37.769Z","service":"Command Processor","environment":"development","host":"N/A","status":{"db":{"status":"BAD"},"overall":"BAD"},"build":"48a26eda\n0.0.0-2268\n","duration":5};
var commandsDataIMissingStatusInvalidFormat = {"time":"2018-08-23T20:26:37.769Z","service":"Command Processor","environment":"development","host":"N/A","build":"48a26eda\n0.0.0-2268\n","duration":5};
var commandsDataIEmpty = {};