const expect = require('chai').expect;
const EndpointTester = require('../modules/endpointTester');


describe('EndpointTester() try create with valid url', function () {
    it('should init ok', function () {

        // 1. ARRANGE
       
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new EndpointTester("http://google.com",false); } ).to.not.throw();

    });
});

describe('EndpointTester() try create with invalid url', function () {
    it('should throw', function () {

        // 1. ARRANGE
       
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new EndpointTester("123.com",false); } ).to.throw();

    });
});

describe('EndpointTester() try create with undefined url', function () {
    it('should throw', function () {

        // 1. ARRANGE
       
        // 2. ACT

        // 3. ASSERT
        expect(function () { new EndpointTester(undefined,false); } ).to.throw();

    });
});