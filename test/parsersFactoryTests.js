const expect = require('chai').expect;
const ParserFactory = require('../parsers/parserFactory');

describe('ParserFactory() try create with undefined url', function () {
    it('should throw', function () {

        // 1. ARRANGE
        const p = new ParserFactory();
       
        // 2. ACT
        // 3. ASSERT
        expect(function () { p.getParserForUrl(undefined) } ).to.throw();

    });
});

describe('ParserFactory() try create with imaginary url', function () {
    it('should throw', function () {

        // 1. ARRANGE
        const p = new ParserFactory();
       
        // 2. ACT
        // 3. ASSERT
        expect(function () { p.getParserForUrl("imaginary url") } ).to.throw();

    });
});

describe('ParserFactory() try create a Bim360dmParser parser', function () {
    it('should get a Bim360dmParser object', function () {

        // 1. ARRANGE
        const p = new ParserFactory();
       
        // 2. ACT
        // 3. ASSERT
        expect(p.getParserForUrl("https://bim360dm-dev.autodesk.com/health?self=true")).to.be.an('object');
    });
});

