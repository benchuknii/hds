const expect = require('chai').expect;
const EndpointHealthTracker = require('../modules/endpointHealthTracker');
const hdsConfig = require('../utils/configurationLoader');

describe('EndpointHealthTracker() try create with invalid url', function () {
    it('should init ok', function () {

        // 1. ARRANGE
       
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new EndpointHealthTracker("123s.com",false); } ).to.throw();

    });
});

describe('EndpointHealthTracker() try create with undefined url', function () {
    it('should init ok', function () {

        // 1. ARRANGE
       
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new EndpointHealthTracker(undefined,false); } ).to.throw();

    });
});


describe('EndpointHealthTracker() try create with a not confgirued url', function () {
    it('should throw', function () {

        // 1. ARRANGE
       
        // 2. ACT
        
        // 3. ASSERT
        expect(function () { new EndpointHealthTracker("http://google.com",false); } ).to.throw();

    });
});

describe('EndpointHealthTracker() init with current config and a valid url', function () {
    it('should create 60 slots for current config', function () {

        // 1. ARRANGE
        const ept = new EndpointHealthTracker("https://bim360dm-dev.autodesk.com/health?self=true",false)
        // 2. ACT
        ept.stop();
        // 3. ASSERT
        expect(ept.slots).to.be.equal(60);

    });
});