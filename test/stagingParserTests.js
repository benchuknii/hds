const expect = require('chai').expect;
const StagingParser = require('../parsers/stagingParser');
const ResultUtil = require('../parsers/resultUtil');

describe('StagingParser() data with all good endpoints', function () {
    it('should run parser and get result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new StagingParser();

        // 2. ACT
        const res = p.parse(stagingDataGood, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.GOOD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("my");
        expect(res.time).to.be.equal("2018-08-23T20:41:18.5794+00:00");
        expect(res.errors.length).to.be.equal(0);

    });
});

describe('StagingParser() data with bad endpoints', function () {
    it('should run parser and get BAD result', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new StagingParser();


        // 2. ACT
        const res = p.parse(stagingDataBad, url);

        // 3. ASSERT
        expect(res.status).to.be.equal(ResultUtil.BAD);
        expect(res.url).to.be.equal(url);
        expect(res.name).to.be.equal("my");
        expect(res.time).to.be.equal("2018-08-23T20:41:18.5794+00:00");
        expect(res.errors.length).to.be.equal(1);

    });
});

describe('StagingParser() data with invalid data', function () {
    it('should run parser and should throw', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new StagingParser();
       
        // 2. ACT
        // 3. ASSERT
        expect(function () { p.parse(stagingDataIMissingStatusInvalidFormat, url) } ).to.throw();

    });
});

describe('StagingParser() data with empty data', function () {
    it('should run parser and should throw', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new StagingParser();
       
        // 2. ACT
        // 3. ASSERT
        expect(function () { p.parse(stagingDataIEmpty, url) } ).to.throw();

    });
});


describe('StagingParser() undefined data', function () {
    it('should run parser and fail [throw]', function () {

        // 1. ARRANGE
        const url = "url-demo";
        const p = new StagingParser();

        // 2. ACT
        // 3. ASSERT
        expect(function () { p.parse(undefined, url) } ).to.throw();

    });
});

var stagingDataGood = {"HealthCheck":{"xmlns:xsd":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance","service":"my","fireflyViewableFileFormats":"dwg,dwf,dwfx,f3d,ipt,sim,obj,idw,iam,vtfx,sim360,ipb,afe,atf,afem,wire,catpart,igs,iges,prt,x_t,x_b,3dm,sat,stp,sldprt,prt,neu,sldasm,ste,step,asm,ige,smt,sab,smb,catproduct,dwt,stl,instructionx,3ds,dae,afef,mfr","exception":{},"status":"Good","date":"2018-08-23T20:41:18.5794+00:00","build":"2018.2.158"}};
var stagingDataBad = {"HealthCheck":{"xmlns:xsd":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance","service":"my","fireflyViewableFileFormats":"dwg,dwf,dwfx,f3d,ipt,sim,obj,idw,iam,vtfx,sim360,ipb,afe,atf,afem,wire,catpart,igs,iges,prt,x_t,x_b,3dm,sat,stp,sldprt,prt,neu,sldasm,ste,step,asm,ige,smt,sab,smb,catproduct,dwt,stl,instructionx,3ds,dae,afef,mfr","exception":{},"status":"BAD","date":"2018-08-23T20:41:18.5794+00:00","build":"2018.2.158"}};
var stagingDataIMissingStatusInvalidFormat = {"HealthCheck":{"xmlns:xsd":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance","service":"my","fireflyViewableFileFormats":"dwg,dwf,dwfx,f3d,ipt,sim,obj,idw,iam,vtfx,sim360,ipb,afe,atf,afem,wire,catpart,igs,iges,prt,x_t,x_b,3dm,sat,stp,sldprt,prt,neu,sldasm,ste,step,asm,ige,smt,sab,smb,catproduct,dwt,stl,instructionx,3ds,dae,afef,mfr","exception":{},"date":"2018-08-23T20:41:18.5794+00:00","build":"2018.2.158"}};
var stagingDataIEmpty = {};