const hdsConfig = require('../utils/configurationLoader');
const EndpointHealthTracker = require('../modules/endpointHealthTracker');

class MonitorService {

    constructor() {
        this.endPointsTrackers = [];
        this.errors = [];
        for (let urlEndpoint of hdsConfig.urls) {

            try {
                this.endPointsTrackers.push(new EndpointHealthTracker(urlEndpoint.url, urlEndpoint.isSoap))
            }
            catch (e) {
                const err = `Cannot create HealthTracker for url: ${urlEndpoint.url} err: ${e}`;
                this.errors.push({ 'url': urlEndpoint.url, 'hasRequestError': true, 'error': err });
                console.error(err);
            }
        }

    }

    getAvailabilityReport() {
        try {
            let summary = {};
            summary.endpointsData = [];

            let totalAvailabilityPercentage = 0;
            for (let ep of this.endPointsTrackers) {
                let res = ep.getServiceAvailabilityPercentage();
                totalAvailabilityPercentage += res.percentage;
                summary.endpointsData.push({
                    'url': ep.url,
                    'AvailabilityPercentage': res.percentage,
                    'maxSamples': Object.values(ep.results).length,
                    'totalActualSamples': res.ActualRequests
                });
            }

            summary.AvgAvailabilityPercentage = totalAvailabilityPercentage / this.endPointsTrackers.length;

            return summary;
        }
        catch (e) {
            return "Error with request...";
        }
    }

    //There is another and more efficient way to implement this
    //instead of requesting all endpoints again - we could access our cache and pull the last request result avalilable 
    //but for the sake of coding and preseting different approaches, I have impleneted another approach - aggregating all requests.
    async getServicesHealthStatuses() {
        try {
            let promises = [];
            let summary = this.errors.slice(0);

            for (let ep of this.endPointsTrackers) {
                let statusPromise = ep.getServiceStatus()
                promises.push(statusPromise.catch(error => {
                    return error
                }));
            }

            let results = await Promise.all(promises);
            for (let result of results) {
                summary.push(result);
            }
            return summary;
        } catch (e) {
            return "Error with request...";
        }
    }
}

module.exports = MonitorService;