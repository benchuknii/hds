const express = require('express')
const app = express()


const MonitorService = require('./services/monitor');

const monitorService = new MonitorService();

app.get('/availability-report', (req, res) => res.json(monitorService.getAvailabilityReport()));
app.get('/statues-report', async (req, res) => {
    try {
        let summary = await monitorService.getServicesHealthStatuses();
        res.json(summary);
    }
    catch (e) {
        res.json(e);
    }
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log('               ');
    console.log('               ');
    console.log('        BIM-360       ');
    console.log('               ');
    console.log(`$$\\   $$\\ $$$$$$$\\   $$$$$$\\`);
    console.log(`$$ |  $$ |$$  __$$\\ $$  __$$\\`);
    console.log(`$$ |  $$ |$$ |  $$ |$$ /  \\__|`);
    console.log(`$$$$$$$$ |$$ |  $$ |\\$$$$$$\\`);
    console.log(`$$  __$$ |$$ |  $$ | \\____$$\\`);
    console.log(`$$ |  $$ |$$ |  $$ |$$\\   $$ |`);
    console.log(`$$ |  $$ |$$$$$$$  |\\$$$$$$  |`);
    console.log(`\\__|  \\__|\\_______/  \\______/`);
    console.log('               ');
    console.log('               ');
    console.log(`Health Dashboard Service -> listening on port ${port}  ヽ(\´▽\`)\/ Yay!`);
});