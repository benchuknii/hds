//***************************
//This is something we would probably want to load from a db / file/ remote server/ env or any other configuration source
//***************************
const runEveryMs = 1000 * 60;
const saveBacklogForMs = 1000 * 60 * 60;

module.exports = {
    'runEveryMs': runEveryMs,
    'saveBacklogForMs': saveBacklogForMs,
    'urls': [{
            url: 'https://bim360dm-dev.autodesk.com/health?self=true',
            'isSoap': false
        },
        {
            url: 'https://commands.bim360dm-dev.autodesk.com/health',
            'isSoap': false
        },
        {
            url: 'https://360-staging.autodesk.com/health',
            'isSoap': true
        }
    ]
};