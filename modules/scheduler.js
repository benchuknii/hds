class Scheduler {

  constructor(runEveryMs, worker, workerName) {

    if (!runEveryMs) {
      throw new Error('runEveryMs must not be undefined');
    }

    console.debug(`run task every ${runEveryMs} ms`);

    const MIN_ALLOWED_FREQ = 100;
    //this min value can be changed but must check for positive number and too low will have a performance impact
    if (runEveryMs <= MIN_ALLOWED_FREQ) {
      throw new Error('runEveryMs must greater than 100 ms');
    }

    if (!worker) {
      throw new Error('worker must not be undefined');
    }
    if (!(typeof worker === 'function')) {
      throw new Error('worker must be a valid function');
    }

    this.worker = worker;
    this.runEveryMs = runEveryMs;
    this.workerName = workerName ? workerName : "";
  }

  start(other) {
    //we might want in the future a more advanced scheduling framework such as: https://www.npmjs.com/package/node-schedule
    // but for the moment set interval will be good enough
    this.intervalObj = setInterval(() => {
      // console.debug(`running worker ${ this.workerName } ...`);
      this.worker.bind(other)();
    }, this.runEveryMs);
    this.worker.bind(other)();//make the first call immediately - we can omit this depending to requirements
  }

  stop() {
    clearInterval(this.intervalObj);
  }

}

module.exports = Scheduler;