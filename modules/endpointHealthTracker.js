const EndPointTester = require('./endPointTester');
const Scheduler = require('./scheduler');
const hdsConfig = require('../utils/configurationLoader');
const ParserFactory = require('../parsers/parserFactory');
const ResultUtil = require('../parsers/resultUtil');

class EndpointHealthTracker {

    constructor(url, isSoapRequest) {

        if (!hdsConfig.saveBacklogForMs) {
            throw new Error('saveBacklogForMs must not be undefined');
        }

        if (hdsConfig.saveBacklogForMs < hdsConfig.runEveryMs) {
            throw new Error('saveBacklogForMs must be at least one unit of');
        }

        console.debug(`Service will be pulled every ${hdsConfig.runEveryMs} ms`);
        console.debug(`Back log size is ${hdsConfig.saveBacklogForMs} ms`);

        //colud have also set the hds config to be a dependency for this class (invertion of control - IOC).
        this.slots = hdsConfig.saveBacklogForMs / hdsConfig.runEveryMs;
        //the results object is our in memory cahce - we could have used db here or persistent in memory db like this one:
        //http://lokijs.org/#/
        this.results = {};
        //assume default state for service is healthy - we could also do other things here - like wait for at least a complete round trip
        for (let i = 0; i < this.slots; i++) {
            this.results[i] = -1;
        }

        console.debug(`Back log slots size is ${this.slots}`);

        let factory = new ParserFactory();
        this.parser = factory.getParserForUrl(url);
        //we could also pass 'runEveryMs' and 'saveBacklogForMs' from the ctor if we wanted each service to have different timings
        //hdsConfig.saveBacklogForMs
        this.url = url;
        this.index = 0;
        this.endPointTester = new EndPointTester(url, isSoapRequest);
        this.scheduler = new Scheduler(hdsConfig.runEveryMs, this.worker);
        this.scheduler.start(this);
    }

    async worker() {

        const callId = this.index;
        try {
            let result = await this.endPointTester.testUrl({
                'parse': true
            })
            if (result.resCode == 200) {
                let parsedRes = this.parser.parse(result.jsonRes, this.url);
                // I did not cache the actual result only of it is good or bad.
                // could have cached the full parsed result and use the last one for the 2nd api - please see Read me file
                // for full explanation and reasons.
                if (parsedRes.status === ResultUtil.GOOD)
                {
                    this.results[callId] = 1;//ok
                }
                else{
                    this.results[callId] = 0;//bad
                }
            } else {
                this.results[callId] = 0;//error
            }
        } catch (e) {
            this.results[callId] = 0;
        }

        this.index++;
        this.index = (this.index) % this.slots;
    }

    async getServiceStatus() {
        try {
            let res = await this.endPointTester.testUrl({
                'parse': true
            });

            if (res.resCode == 200) {
                return {
                    'url': this.url,
                    'data': this.parser.parse(res.jsonRes, this.url),
                    'hasRequestError': false
                };
            } else {
                return Promise.reject({
                    'url': this.url,
                    'data': ResultUtil.generateError(res.jsonRes, this.url),
                    'hasRequestError': true
                });
            }
        }
        catch (err) {
            return Promise.reject({
                'url': this.url,
                'data': ResultUtil.generateError(err.toString(), this.url),
                'hasRequestError': true
            });
        }
    }

    getServiceAvailabilityPercentage() {
        let values = Object.values(this.results);
        if (!values || values.length <= 0) {
            return 0; //we could also throw here an exception
        }
        let goodRequests = 0;
        let totalRequests = 0;
        for (let item of values) {
            if (item === -1) {
                continue;
            }
            if (item === 1) {
                goodRequests++;
            }
            totalRequests++;

        }
        const res = { 'ActualRequests': 0, 'percentage': 0 };
        if (totalRequests === 0) {
            return res;
        }
        res.ActualRequests = totalRequests;
        res.percentage = (goodRequests / totalRequests) * 100;
        return res;
    }

    stop()
    {
        this.scheduler.stop();
    }
}

module.exports = EndpointHealthTracker;