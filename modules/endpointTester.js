const request = require('request');
const urlvalidetor = require('valid-url');
const xmlParser = require('xml2json');

class EndpointTester {
    constructor(url, isSoapRequest) {

        if (!url) {
            throw new Error('url must not be undefined');
        }

        if (!urlvalidetor.is_http_uri(url) && !urlvalidetor.is_https_uri(url)) {
            throw new Error('invalid url format');
        }

        this.url = url;
        this.isSoapRequest = isSoapRequest ? isSoapRequest : false; //default is json
    }

    //we could also add timeout for each http request and handle timeout as errors.
    //or wrap the object with timeoutable promise  -- see at the end of the file timeoutable possiable implementation
    testUrl(options) {
        //I used promise to wrap the http request - could have used different lib that supports promise and use await
        return new Promise((resolve, reject) => {
            request(this.url, {
                json: !this.isSoapRequest
            }, (err, res, body) => {
                if (err) {
                    let reason = new Error(err);
                    return reject(reason);
                }
                let resCode = res && res.statusCode;
                var jsonRes = {};
                //console.log('statusCode:', resCode);
                if (resCode == 200) {
                    let contentType = res.headers['content-type'];
                    let isSoap = contentType ? contentType.includes("text/xml") : false;

                    if (options.parse) {
                        if (this.isSoapRequest || isSoap) {
                            jsonRes = JSON.parse(xmlParser.toJson(body));
                        } else {
                            jsonRes = body;
                        }
                    }
                }

                let result = {
                    'resCode': resCode,
                    'jsonRes': jsonRes
                }
                return resolve(result);
            });
        });
    }
}

module.exports = EndpointTester;


// function timeoutable(timeout, func, binding=null) {
//     return (...args)=>{
//         return new Promise(async (resolve, reject) => {
//             let canceled = false
//             const timeoutId = setTimeout(() => {
//                 canceled = true
//                 return reject('timeout')
//             }, timeout)
//             try {
//                 // func = func.bind(null, args)
//                 const result = await func.apply(binding,args)
//                 clearTimeout(timeoutId)
//                 if (!canceled) {
//                     return resolve(result)
//                 }
//             } catch (err) {
//                 return reject(err)
//             }
//         })
//     }

// }