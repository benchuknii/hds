# HDS - Health Dashboard Service




            $$\   $$\ $$$$$$$\   $$$$$$\        
            $$ |  $$ |$$  __$$\ $$  __$$\       
            $$ |  $$ |$$ |  $$ |$$ /  \__|      
            $$$$$$$$ |$$ |  $$ |\$$$$$$\        
            $$  __$$ |$$ |  $$ | \____$$\       
            $$ |  $$ |$$ |  $$ |$$\   $$ |      
            $$ |  $$ |$$$$$$$  |\$$$$$$  |      
            \__|  \__|\_______/  \______/ 

                              
                              
                              




# Note 1:

This is a bit over design - on the other side of the scale I could have just setup a monitor class with a setInterval and 'foreach of the endpoints' call http request then parse and save the results all in one class - this could do the trick, implemeted fastly for a POC and will be just fine for some cases.

BUT ...

* This is an assignment :)
* The following design will allow us to scale and update the code to support the following
	* Allow each service to sample in a different rate and a different results cache depth.
    * Allow the schedualr to be update to something more advanced, such as: https://www.npmjs.com/package/node-schedule This will allow scheduling in specific hours,times,day of week, etc. and also allows us to start and stop each service health check independently!
    * Each endpoint has a parser logic which can be modified without affecting other endpoints, this will allow us to update what is a GOOD or BAD state for a specific endpoint in one file with our affecting the others.
	* 'Endpoint Tester' wraps the http request and parsing logic - this way if we want to use different http library or have different error handling - only this class will be affected.
	* 'Endpoint Health tracker' is the controller of the endpoint health checks - it inits owns the scheduler and the tester and connects them in a uncoupled way - and caching the results as needed
	* Config - must have for any project :)
    

# Note 2:

For the http://localhost:3000/statues-report api - I have no used the cached results intentionally. Of course I could have used the last cached result and return it instead of requesting a new one, did this for 2 reasons:
1.  This is an assignment :) - demonstrate multiple requests sync.
2.  This way I have an updated results and I am decoupled from the cache refresh rate and scheduling system. and the performance penalty is not ugue for this case, if we had a lot of endpoints (more then our 3 at this example) I would probably use the cache results - to speed up the api significantly.



# Now for the HDS real Read Me :)
This project will monitor a set for known and predefined services.

* Calling http://localhost:3000/statues-report will return foreach service an object with the following structure:

        {
            url: "url of endpoint",
            data: {
                status: "summary status - can be GOOD (healthy) or BAD (unhealthy)",
                time: "time taken from endpoint result",
                name: "name of service on it's origin [can be empty]",
                url: "url of endpoint"
            },
            hasRequestError: boolean - true if had any request errors such as no network 404 or other.
            errors: [array of strings aggregating all parsed errors from endpoint - will be empty array if stats is GOOD if status is BAD there will be a minimum of one error]
        }

* Calling http://localhost:3000/availability-report will return an aggregated availability and a descriptive result foreach endpoint 

structure is as follows:

        errors: [ array for strings with error/s for each endpoint that had a problem],
        endpointsData: [array of object describing the endpoint status],
        AvgAvailabilityPercentage: and aggregated result averaging all endpoints availabilities 
    
        endpingData structure is:
        {
            url: "endpoint url",
            AvailabilityPercentage: number -> (successfully completed health checks) / (total number of samples taken so far),
            maxSamples: length of cached results,
            totalActualSamples: number -> actual samples taken so far - when inspected near the time of HSD startup - the value should increase at the scheduler rate until reaching maxSamples value.
        }
       
        
# Top level design

![top level design](design-top-level.jpg)

# Scheduled health check sequence (always running while service is up)

![scheduled health check sequence](scheduled_health_check.jpg)

# API (1) -  Get Services Health Statuses sequence

![Get Services Health Statuses Statuses](GetServicesHealthStatuses.jpg)

# API (2) -  Get Availability Report sequence

![Get Availability Report sequence](GetAvailabilityReport.jpg)


# Build
    npm install

# Test
    npm run test

# Run
    npm run start