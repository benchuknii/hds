
const urlvalidetor = require('valid-url');
const Bim360dmParser = require('./bim360dmParser');
const CommandsParser = require('./commandsParser');
const StagingParser = require('./stagingParser');

class ParserFactory {

    constructor() {
  
        this.parsers = {};
        this.parsers['https://bim360dm-dev.autodesk.com/health?self=true'] = new Bim360dmParser();
        this.parsers['https://commands.bim360dm-dev.autodesk.com/health'] = new CommandsParser();
        this.parsers['https://360-staging.autodesk.com/health'] = new StagingParser();
    
    }
  
    getParserForUrl(url) {

      if(!url)
      {
        throw new Error('url must not be undefined');
      }

      if (!urlvalidetor.is_http_uri(url) && !urlvalidetor.is_https_uri(url)) {
        throw new Error('invalid url format');
      }


      let p = this.parsers[url];

      if(!p)
      {
        throw new Error('no parser for this url: ' + url);
      }

      return p;
    }
  
  }
  
  module.exports = ParserFactory;