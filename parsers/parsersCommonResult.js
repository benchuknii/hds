const resultTemplate = {
  'status': undefined,
  'time': undefined,
  'name': undefined,
  'url':undefined,
  'errors':[]
};

module.exports = function()
{
  //clone
  let obj =  Object.assign({}, resultTemplate);
  obj.errors = [];
  return obj;
}