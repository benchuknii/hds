const parseResult = require('./parsersCommonResult');
const ResultUtil = require('./resultUtil');

class Bim360dmParser {

  constructor() {
    this.GOOD = "GOOD";
    this.BAD = "BAD";
  }

  parse(json, url) {

    if (!json) {
      throw new Error('json must not be undefined');
    }

    let result = parseResult()
    result.status = ResultUtil.GOOD;
    result.url = url;
    result.name = json.service;
    result.time = json.time;

    if (!json.status)
    {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD status object - interface has changed?");
      return result;
    }

    if ( json.status.overall !== this.GOOD) {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD over all state");
    }

    if (json.status.active_record !== this.GOOD) {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD active record");
    }

    if (json.status.redis !== this.GOOD) {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD redis state");
    }

     //if all properties must exsits then we should remove  val.'fieldName' && from all if statments.
    for (let w of json.status.workers) {
      for (let val of Object.values(w)) {

        if (val.issue_ping && val.issue_ping !== this.GOOD) {
          result.status = ResultUtil.BAD;
          result.errors.push("BAD worker [issue_ping] state: " + Object.keys(w)[0]);
        }

        if (val.dos_ping && val.dos_ping !== this.GOOD) {
          result.status = ResultUtil.BAD;
          result.errors.push("BAD worker [dos_ping] state: " + Object.keys(w)[0]);
        }

        if (val.folder_notification && val.folder_notification !== this.GOOD) {
          result.status = ResultUtil.BAD;
          result.errors.push("BAD worker [folder_notification] state: " + Object.keys(w)[0]);
        }

        if (val.acm && val.acm !== this.GOOD) {
          result.status = ResultUtil.BAD;
          result.errors.push("BAD worker [acm] state: " + Object.keys(w)[0]);
        }

        if (val.oss && val.oss !== this.GOOD) {
          result.status = ResultUtil.BAD;
          result.errors.push("BAD worker [oss] state: " + Object.keys(w)[0]);
        }

      }
    }

    return result;
  }
}

module.exports = Bim360dmParser;