const parseResult = require('./parsersCommonResult');
const ResultUtil = require('./resultUtil');

class StagingParser {

  constructor() {
    this.GOOD = "Good";
    this.BAD = "Bad";
  }
  
  parse(json, url) {

    if (!json) {
      throw new Error('json must not be undefined');
    }

    let result = parseResult()

    
    result.status = ResultUtil.GOOD;
    result.url = url;
    result.name = json.HealthCheck.service;
    result.time = json.HealthCheck.date;

    if (json.HealthCheck.status === undefined) {
      throw new Error('invalid format');
    }

    if (json.HealthCheck.status !== this.GOOD) {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD top level status");
    }

    return result;
  }

}

module.exports = StagingParser;