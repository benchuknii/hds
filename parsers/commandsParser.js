const parseResult = require('./parsersCommonResult');
const ResultUtil = require('./resultUtil');

class CommandsParser {

  constructor() {
    this.GOOD = "OK";
    this.BAD = "BAD";
  }
  
  parse(json, url) {

    if (!json) {
      throw new Error('json must not be undefined');
    }

    let result = parseResult()
    result.status = ResultUtil.GOOD;
    result.url = url;
    result.name = json.service;
    result.time = json.time;

    if (json.status.overall !== this.GOOD) {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD over all state");
    }

    if (json.status.db === undefined  || json.status.db.status !== this.GOOD) {
      result.status = ResultUtil.BAD;
      result.errors.push("BAD db status");
    }

    return result;
  }

}

module.exports = CommandsParser;