const parseResult = require('./parsersCommonResult');

module.exports = {
    'generateError': function (error, url)
    {
      let result = parseResult();
      result.status = ResultUtil.BAD;
      result.url = url;
      result.name = url;
      result.time = new Date();
      result.errors.push(error);
      return result;
    },
    'GOOD':'GOOD',
    'BAD':'BAD'
  };